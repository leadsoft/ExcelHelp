//原生js修改样式
(function(){
    var getparastr = function (strname) {
        var hrefstr, pos, parastr, para, tempstr;
        hrefstr = window.location.href;
        pos = hrefstr.indexOf("?")
        parastr = hrefstr.substring(pos + 1);
        para = parastr.split("&");
        tempstr = "";
        for (i = 0; i < para.length; i++) {
            tempstr = para[i];
            pos = tempstr.indexOf("=");
            if (tempstr.substring(0, pos) == strname) {
                return tempstr.substring(pos + 1);
            }
        }
        return null;
    }
    var style = getparastr("_style") || "lumen";
    style = style.replace('#','');
    var oHead = document.getElementsByTagName('HEAD').item(0);
    var mainindex = 0;
    for(var i=0;i<oHead.childNodes.length;i++){
        //console.log(oHead.childNodes[i]);
        if(oHead.childNodes[i].attributes && oHead.childNodes[i].attributes.href){
            var href = oHead.childNodes[i].attributes.href.nodeValue;
            if(href.indexOf("bootstrap.min.css")>0){
                //href = href.replace("bootstrap.min.css",style+".css");
                //oHead.childNodes[i].setAttribute("href",href)
                mainindex=i+1;
            }
        }
    }
    //得到当前路径
    var pathName = window.document.location.pathname;
    var pash = "";
    //去除第一个得到路径层级
    for (var i = 0; i < pathName.substr(0, 1).split("/").length; i++) {
        //一级一个 ../
        pash += "../";
    }

    //新增link引用
    var olink = document.createElement("link");
    olink.rel = "stylesheet"
    olink.type = "text/css";
    olink.href = "css/"+style+".css";
    //oHead.appendChild(olink);
    oHead.insertBefore(olink,oHead.childNodes[mainindex]);

    var olink = document.createElement("link");
    olink.rel = "stylesheet"
    olink.type = "text/css";
    olink.href = pash+"css/"+style+".css";
    //oHead.appendChild(olink);
    oHead.insertBefore(olink,oHead.childNodes[mainindex]);

})();
(function ($) {
    //通过renderto 得到控件
    window.bsEx = function (renderto) {
        return bsEx.ui[renderto];
    };

    $.fn.bsEx="2.0.2";

    //基础属性及方法
    (function (ex) {
        //ui基础包
        ex.ui = function(op){
            if(!op){return};
            if (ex.isObj(op)) {
                if(ex.isJQDom(op)){
                    var bsExid = $(op).attr("bsExid");
                    if(bsExid && bsExid!=""){
                        return ex.ui[bsExid];
                    }
                }else{
                    return ex.ui[op.id];
                }
            }else if(ex.isStr(op)){
                return ex.ui[op];
            }
        };
        ex.ui.nub=0;
        //得到一个计算的ID
        ex.getId=function(){
            ex.ui.nub++;
            return "bsex" + ex.ui.nub;
        };
        ex.ui.base=function(){
            this.language = ex.language("zhCN");
            //this.id = ex.getId();//控件ID
        };
        //事件
        ex.ui.base.eve = function(){
            var _eve = {
                eve:{},
                //注册事件
                on:function(ename,fun){
                    var t = this;
                    var dom = t.renderto || t.id;
                    var enames = ename.split(" ");
                    //$.each(enames, function(n, name) {
                    //    if (name) {
                    //        $(dom).on(name,fun);
                    //    }
                    //});
                    $.each(enames, function(n, name) {
                        if (name) {
                            if(!t.eve[name]){t.eve[name]=[];}
                            t.eve[name].push(fun);
                        }
                    });
                },
                //触发事件
                trigger:function(ename,args){
                    var t = this;
                    var dom = t.renderto || t.id;
                    //如果触发的是初始完毕事件
                    if(ename=="beforeinit"){
                        if(!t.id || t.id==""){t.id = ex.getId();}//默认ID
                        if(t.cls){t.addClass(t.cls);}//添加自定义class
                        if(t.id && t.id!=""){//为绘制的元素绑定ID
                            $(t.renderto).attr("bsExid",t.id);
                        }
                    }
                    if(ename=="inited"){
                        ex.ui[t.id] = t;//向ui类中添加该控件
                    }
                    //参数
                    var r = $.makeArray(args);
                    //触发属性on事件
                    if(t['on'+ename]){ex.doCallback(t['on'+ename],r);}
                    //触发页面方法id+on+事件名称
                    try {
                        eval('ex.doCallback('+ t.id + '_on' + ename+',r)');
                    } catch (ex) { }
                    //触发domJQ原生事件
                    //$(dom).trigger(ename,r);
                    //触发由on注册的事件
                    if(t.eve[ename] && t.eve[ename].length>0){
                        $.each(t.eve[ename], function(n, fn) {
                            ex.doCallback(fn,r);
                        });
                    }
                    if(ename!='alleve'){
                        t.trigger('alleve',[ename,args])
                    }
                }
            };
            return _eve;
        };
        //基础属性
        ex.ui.base.prototype = {
            renderto:"",//绘制ID
            type: "base",//控件类型
            firstInit: false,//第一次加载是否完成
            language: null,
            attr:function(name,val){
                var t = this;
                if(val){
                    $(t.renderto).attr(name,val);
                }else{
                    return $(t.renderto).attr(name);
                }
            },
            addClass:function(val){
                var t = this;
                $(t.renderto).addClass(val);
            },
            init: function () { },//绘制方法
            onbeforeinit:function(){},//绘制前事件
            oninited:function(){}//绘制后事件
        };
        //$.extend(ex.ui.base.prototype, new ex.ui.base.eve());
        //回调方法
        ex.doCallback = function (fn, args) {
            return fn.apply(this, args);
        }
        //是否为字符串
        ex.isStr=function(obj){
           return $.type(obj) === "string"
        };
        //是否为对象
        ex.isObj=function(obj){
            return $.type(obj) === "object" 
        };
        //是否Jq对象
        ex.isJQDom=function(obj){
            return obj instanceof jQuery;
        };
        //是否为数字
        ex.isNub=function(obj){
            return $.type(obj) === "number" && !isNaN(obj)
        };
        //是否为方法
        ex.isFun = function(obj){
            return $.isFunction(obj);
        }

        //注册点击事件
        ex.initNavbarTab= function(navbar,tab,op){
            //注册菜单点击事件
            navbar.meunel.find('a[data-url]').on('click',function(){
                var url= $(this).attr('data-url');
                var text= $(this).text();
                var id= $(this).parent().attr('id') +"-tab";
                op = op || {};
                var _tab =  $.extend(op, {id:id,url:url,title:text,active:true});
                //var _tab =  {url:url,title:text};

                _tab.navbarhide = navbar.id;
                tab.addTab(_tab);
                navbar.selectItem( $(this).parent().attr('id'));
            });
            //注册外部点击事件 隐藏菜单
            window.navbarhide =function (id) {
                if(bsEx.ui[id] && bsEx.ui[id].mtrigger){
                    $(bsEx.ui[id].renderto).trigger('click',[]);
                }
            };
            $(navbar.meunel).find("li").mouseover(function(){
                ex.ui[navbar.id].mtrigger=false;
            });
            $(navbar.meunel).find("li").mouseout(function(){
                ex.ui[navbar.id].mtrigger=true;
            });
        }

        //窗口大小改变
        ex.winresizeend=function(fun,args){
            var rtime = new Date(1, 1, 2000, 12,00,00);
            var timeout = false;
            var delta = 200;
            args = args||[];
            $(window).resize(function() {
                rtime = new Date();
                if (timeout === false) {
                    timeout = true;
                    setTimeout(resizeend, delta);
                }
            });
    
            function resizeend() {
                if (new Date() - rtime < delta) {
                    setTimeout(resizeend, delta);
                } else {
                    timeout = false;
                    ex.doCallback(fun, args);
                }
            }
        };
        //得到页面参数
        ex.getparastr = function (strname) {
            var hrefstr, pos, parastr, para, tempstr;
            hrefstr = window.location.href;
            pos = hrefstr.indexOf("?")
            parastr = hrefstr.substring(pos + 1);
            para = parastr.split("&");
            tempstr = "";
            for (i = 0; i < para.length; i++) {
                tempstr = para[i];
                pos = tempstr.indexOf("=");
                if (tempstr.substring(0, pos) == strname) {
                    return tempstr.substring(pos + 1);
                }
            }
            return null;
        }
    })(bsEx);

    //语言包
    (function(ex,w) {
        ex.language = function(){};
        ex.language.zhCN = {
            Dropdownlist: {
                selectnoneText: function () { return '-----请选择-----'; },
                deselectAllText: function () { return '清空'; },
                selectAllText: function () { return '全选'; }
            },
            Modal: {
                title: function () { return "窗口标题"; },
                closebtn: function () { return "关闭"; }
            },
            Tab: {
                close:function(){return "关闭当前"},
                closeother:function(){return "关闭其他标签"},
                closeall:function(){return "关闭全部标签"},
            },
            Navbar: {
                title:function(){return "网页标题";}
            },
            ItemForm:{
                valmsg:function(){return "请填写"},
            },
            Panel:{
                title:function(){return "标题";}
            },
            Button:{
                text:function(){return "按钮";},
                loadingtxt:function(){return "加载中..."}
            },
            BtnGroup:{},
            Grid:{
                formatLoadingMessage: function () {
                    return '正在努力地加载数据中，请稍候……';
                },
                formatRecordsPerPage: function (pageNumber) {
                    return '每页显示 ' + pageNumber + ' 条记录';
                },
                formatShowingRows: function (pageFrom, pageTo, totalRows) {
                    return '显示第 ' + pageFrom + ' 到第 ' + pageTo + ' 条记录，总共 ' + totalRows + ' 条记录';
                },
                formatSearch: function () {
                    return '搜索';
                },
                formatNoMatches: function () {
                    return '没有找到匹配的记录';
                },
                formatPaginationSwitch: function () {
                    return '隐藏/显示分页';
                },
                formatRefresh: function () {
                    return '刷新';
                },
                formatToggle: function () {
                    return '切换';
                },
                formatColumns: function () {
                    return '列';
                },
                formatExport: function () {
                    return '导出数据';
                },
                formatClearFilters: function () {
                    return '清空过滤';
                }
            }
        };
        //默认语言包
        ex.delanguage =function(op){
            if(op && ex.isObj(op)){
                return bsEx.ui.language = $.extend(ex.language.zhCN,op);
            }else{
               return bsEx.ui.language = ex.language.zhCN;
            }
        };
        ex.getUIlanguage = function(type){
            var l =  bsEx.ui.language[type];
            var t = {};
            for(var b in l){
                if(b && type=="Grid" && b.indexOf("format")>=0){
                    t[b] = l[b];
                }
                else if(b){
                   t[b] = l[b]();
                }
            }
            return t;
        };
    })(bsEx, window);

    //Modal 模态框 原创
    //依赖 ex.ui.base bootstrap原生Modal
    (function (ex) {
        var exb = new ex.ui.base();
        var _Modal = function (op) {
            var t = this;
            //继承事件基类
            $.extend(t,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));

            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            return t;
        }
        var language =  ex.delanguage().Modal;
        _Modal.prototype = $.extend(exb, {
            type:"Modal",
            firstInit: false,
            body: null,//填充到body元素
            footer: null,//填充到footer元素
            headerel: null,//头元素
            bodyel: null,//body元素
            footerel: null,//footer元素
            btns: [],//按钮组
            title: language.title(),//title
            closebtn: language.closebtn(),//title
            showclosebtn: true,//显示关闭按钮
            initMax: true,//是否最大化
            //dandanwa:false,//是否在失去焦点后关闭
            setHeigth: function (h) {//设置高度
                var t = this;
                $(t.renderto).find('.modal-body').css('min-height', h);// - 110 * 2
                $(window).resize(function () {
                    $(t.renderto).find('.modal-body').css('min-height', h);// - 110 * 2
                });
            },
            setWidth: function (w) {//设置宽度
                var t = this;
                $(t.renderto).find('.modal-dialog').css('width', w);// - 200 * 2
                $(window).resize(function () {
                    $(t.renderto).find('.modal-dialog').css('width', w);// - 200 * 2
                });
            },
            modal: { show: true, backdrop: 'static' },
            doLayout:function(){
                var t = this;
                if (t.initMax) {
                    var w = $(window).width();
                    var h = $(window).height();
                    //console.log(w +"|"+h)
                    $(t.renderto).find('.modal-dialog').css('width', '95%');// - 200 * 2
                    $(t.renderto).find('.modal-body').css('min-height', h-h*0.3);// - 150 * 2
                }
            },
            init: function (isshow) {
                var t = this;
                //ex.doCallback(t.onbeforeinit, [t]);
                $(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                var body = t.body;
                t.headerel = null;
                t.bodyel = null;
                t.footerel = null;
                if (!$(t.renderto).hasClass('modal')) {
                    $(t.renderto).addClass('modal');
                }
                if (!$(t.renderto).hasClass('fade')) {
                    $(t.renderto).addClass('fade');
                }
                $(t.renderto).append('<div class="modal-dialog"><div class="modal-content"></div></div>');
                t.headerel = $('<div class="modal-header"></div>');
                if (t.showclosebtn) {
                    $(t.headerel).append('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>');
                }
                $(t.headerel).append('<div class="modal-title">' + t.title + '</div>');
                $(t.renderto).find(".modal-content").append(t.headerel);
                t.bodyel = $('<div class="modal-body"></div>');
                $(t.renderto).find(".modal-content").append(t.bodyel);
                t.bodyel.append(body);
                t.footerel = $('<div class="modal-footer"></div>');
                $(t.renderto).find(".modal-content").append(t.footerel);
                t.btns.forEach(function (btn) {
                    var btnclass = btn.class || "btn-primary";
                    var _b = "";
                    if (btn.id != "closebtn") {
                        _b = $('<button class="btn" type="button"  id="' + btn.id + '">' + btn.text + '</button>');
                        $(_b).addClass(btnclass);
                    } else {
                        _b = $('<button class="btn btn-default" type="button" data-dismiss="modal" aria-hidden="true" id="' + t.id + '-' + btn.id + '_">' + t.closebtn + '</button>');
                    }
                    $(t.footerel).append(_b);
                }, this);
                
                t.firstInit = true;
                //布局 
                t.doLayout();
                
                //立即显示
                if (isshow) {
                    var moopt = { show: true, backdrop: 'static' };//keyboard
                    moopt = $.extend(moopt, t.modal);
                    $(t.renderto).modal(moopt);
                }

                //注册事件
                $(t.renderto).on('show.bs.modal', function () {
                    t.trigger('beforeshow',[t]);
                })
                $(t.renderto).on('shown.bs.modal', function () {
                    t.trigger('showed',[t]);
                })
                $(t.renderto).on('hide.bs.modal', function () {
                    t.trigger('beforehide',[t]);
                })
                $(t.renderto).on('hidden.bs.modal', function () {
                    t.trigger('hidden',[t]);
                })
                //ex.doCallback(t.onFirstInited, [t]);
                t.trigger('inited',[t]);
                return t;
            },
            setTitle: function (title) {//设置标题
                var t = this;
                t.title = title;
                $(t.headerel).find('.modal-title').html(t.title);
            },
            toggle: function () {//设置是否显示
                var t = this;
                $(t.renderto).modal('toggle');
            },
            show: function () {//显示
                var t = this;
                if (!t.firstInit) {
                    t.init();
                }
                
                t.doLayout();//布局 
                t.trigger('beforeshow',[t])
                var moopt = { show: true, backdrop: 'static' };//keyboard
                moopt = $.extend(moopt, t.modal);
                $(t.renderto).modal(moopt);
            },
            hide: function () {//关闭
                var t = this;
                $(t.renderto).modal('hide');
            }
        });

        //title，message，0，function
        _Alert = function(){
            //var t = this;
            //arguments
            var _op = {};
            var second = 0;
            var fun = function(){};
            var mid = ex.getId();
            $(document.body).append('<div id="'+mid+'"></div>');
            _op.renderto="#"+mid; 
            _op.initMax=false;
            //如果参数是一个
            if(arguments.length==1){
                //如果是字符串就直接显示
                if(ex.isStr(arguments[0])){
                    _op.body = arguments[0];
                    _op.title ="";
                }
                //如果是对象就是配置信息
                else if (ex.isObj(arguments[0])){
                    var op = arguments[0];
                    _op.title =op.title || "";
                    _op.body =op.message || "";
                    second = op.second || second;
                    fun = op.fun || fun;
                }
            }
            //如果参数是大于等于两个，前两个是标题和内容
            if(arguments.length>=2){
                _op.title = arguments[0];
                _op.body = arguments[1];
            }
            //如果参数大于等于三个，
            if(arguments.length>=3){
                //如果第三个是数字就是显示时间
                if(ex.isNub(arguments[2]) && arguments[2]!=0){
                    second = arguments[2];
                }
                //如果第三个是方法就是回调
                else if(ex.isFun(arguments[2])){
                    fun = arguments[2];
                }
            }
            //如果参数大于等于4个
            if(arguments.length>=4){
                //如果第四个是方法 就是回调
                if(ex.isFun(arguments[3])){
                    fun = arguments[3];
                }
            }
            //_op.btns=[{ id: "closebtn" }];
            var m = new _Modal(_op);
            m.on("hidden",function(){
                //关闭时销毁控件 并回调
                $("#"+mid).remove();
                ex.doCallback(fun,[]);
            })
            m.show();
            if(second!=0){
                setTimeout(function(){
                    m.hide();
                }, second*1000);
            }
        }
        ex.Modal = _Modal;
        ex.Alert = _Alert;
    })(bsEx, window);

    //Tab 页签 原创
    //依赖 ex.ui.base
    (function (ex) {
        var exb = new ex.ui.base();
        //var exeve = new ex.ui.base.eve();
        //exb = $.extend(exb,exeve);
        var _Tab = function (op) {
            var t = this;
            //继承事件基类
            $.extend(t,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            return t;
        }
        var language =  ex.delanguage().Tab;
        var items = function () {
            var _tab = {
                id: ex.getId()+"-tab",//id
                title: "",//标题
                url: "",//地址
                isiframe: false,//是否生成ifram
                active: false,//是否激活
                iframe: null,//ifram元素
                iframefn: null,//返回ifram 的中的contentWindow对象 执行function 返回
                tabel: null,//页签元素
                bodyel: null,//body元素
                load: null,//load 事件触发
                showclosebtn: false,//是否显示关闭按钮
                doLayout:function(_tab){
                    var t = this;
                    if(t.isiframe){
                        var iframesize =function(thistab){ 
                            var pheight = $(thistab.renderto).height();
                            var iftop =  $(thistab.renderto).find('.nav.nav-tabs').height();
                            t.iframe.css('height',pheight - iftop);
                            //console.log( t.iframe.attr("id")+"高度"+ t.iframe.css('height'));
                        };
                        setTimeout(function(){
                            iframesize(_tab);
                        },200)
                        ex.winresizeend(iframesize,[_tab]);
                    }
                }
            };
            $.extend(_tab,new ex.ui.base.eve()); 
            return _tab;
        }
        var addtab = function(t,tab){
            
            t.trigger('beforeadd',[t]);
            tab.tabel = $('<li toggleid = '+(tab.id)+'> <a data-toggle="tab" href="#' + tab.id + '" >' + tab.title + '</a>'); 
            var closeicon = $('<i class="glyphicon glyphicon-remove small tab_close" tabsindex="' + (tab.id) + '" ></i>');
            if (tab.showclosebtn) {
               tab.tabel = $('<li toggleid = '+(tab.id)+'> <a data-toggle="tab" class="tab_title" href="#' + tab.id + '" >' + tab.title
                    + '&nbsp;'+closeicon[0].outerHTML+'</a></li>');
            }
            var bodyel = tab.bodyel;
            tab.bodyel = $('<div class="tab-pane" id="' + tab.id + '"></div>');
            if (tab.isiframe) {
                if(bsEx.getparastr("_style")){
                    tab.url = tab.url.indexOf("?")>0?tab.url+"&_style="+bsEx.getparastr("_style").replace('#',''):tab.url+"?_style="+bsEx.getparastr("_style").replace('#','');
                }
                tab.iframe = $('<iframe id="' + tab.id + '_iframe" width="100%" height="100%" src="' + tab.url + '" frameborder="0"></iframe>');
                $(tab.bodyel).append(tab.iframe);
            }
            if (t.fade) {
                $(tab.bodyel).addClass("fade");
                $(tab.bodyel).addClass("in");
            }
            $(t.navtabs).append(tab.tabel);
            $(t.tabcontent).append(tab.bodyel);
            tab.bodyel.append(bodyel);
            
            if (document.getElementById(tab.id + '_iframe') && document.getElementById(tab.id + '_iframe').contentWindow) {
                tab.iframefn = document.getElementById(tab.id + '_iframe').contentWindow;
            }
            tab.iframe = $('#' + tab.id + '_iframe');
            tab.renderto = '#' + tab.id + '_iframe';
            if(tab.navbarhide && tab.navbarhide!=''){
                tab.on('ifloaded',function(t,e){
                    //如果绑定了导航栏，注册点击事件 隐藏导航菜单
                    ex.doCallback(function(){
                        $(e.target.contentDocument).on('click',function(){
                           parent.window.navbarhide(tab.navbarhide);
                        });
                    },[]);
                })
            }
            
            tab.iframe.load(function (e) {
                if(tab.load){
                    ex.doCallback(tab.load,[this]);
                }
                if ($(tab.iframe ).attr('src') && $(tab.iframe ).attr('src') != "" && $(tab.iframe ).attr('src') != "undefined") {
                    tab.doLayout(t);
                    tab.trigger('ifloaded',[t,e]);
                }
            });
            //var load;
            //if (tab.load) {
            //    load = tab.load;
            //    $('#' + tab.id + '_iframe').load(function () {
            //        load(this);
            //        tab.trigger('ifloaded',[t]);
            //    });
            //}
            
            $('i[tabsindex="'+tab.id+'"]').on('click', function () {
                if($('i[tabsindex="'+tab.id+'"]').length>0){
                    t.hide(tab.id);
                    return false;
                }
            })
            
            t.trigger('added',[t,tab]);
        }
        _Tab.prototype = $.extend(exb, {
            type:"Tab",
            navtabs: null,//头部
            tabcontent: null,
            tabs: [],
            right:[],
            close:language.close(),
            closeother:language.closeother(),
            closeall:language.closeall(),
            fade: true,//是否显示过度效果
            showrmenu:true,//是否显示右键菜单
            firstInit: false,
            doLayout:function(){
                var t = this;
                t.iframesize();
            },
            iframesize:function(){//重置iframe布局
                var t = this;
                var iframesize =function(thistab){ 
                        var pheight = $(thistab.renderto).height();
                        var iftop =  $(thistab.renderto).find('.nav.nav-tabs').height();
                        $(thistab.renderto).find('.tab-pane').find('iframe').css('height',pheight - iftop);
                };
                setTimeout(function(){
                    iframesize(t);
                },200)
                ex.winresizeend(iframesize,[t]);
            },
            //显示
            show: function (_tab) {
                var t = this;
                /*var tabindex = t.getTabIndex(tab);
                $(t.renderto).find('li').removeClass('active');
                $(t.renderto).find('div.tab-pane').removeClass('active');
                if ($(t.renderto).find('li').eq(tabindex).css("display") == 'none') {
                    $(t.renderto).find('li').eq(tabindex).css("display", "inline");
                }
                $(t.renderto).find('li').eq(tabindex).addClass('active');
                $(t.renderto).find('div.tab-pane').eq(tabindex).addClass('active');
                if ($(t.renderto).find('div.tab-pane').eq(tabindex).hasClass('fade') && !$(t.renderto).find('div.tab-pane').eq(tabindex).hasClass('in')) {
                    $(t.renderto).find('div.tab-pane').eq(tabindex).addClass('in');
                }*/
                var tab = t.getTab(_tab);
                $(t.renderto).find('li').removeClass('active');
                $(t.renderto).find('div.tab-pane').removeClass('active');
                if ($(t.renderto).find('li[toggleid="'+tab.id+'"]').css("display") == 'none') {
                    $(t.renderto).find('li[toggleid="'+tab.id+'"]').css("display", "inline");
                }
                $(t.renderto).find('li[toggleid="'+tab.id+'"]').addClass('active');
                $(t.renderto).find('div#'+tab.id).addClass('active');
                if ($(t.renderto).find('div#'+tab.id).hasClass('fade') && !$(t.renderto).find('div#'+tab.id).hasClass('in')) {
                    $(t.renderto).find('div#'+tab.id).addClass('in');
                }
                //if (t.tabs && t.tabs[tabindex] && t.tabs[tabindex].id) {
                    //eval("var fun;try{fun=" + t.tabs[tabindex].id + "_onactive;}catch(ex){}");
                    //if (fun) {
                    //    fun(t);
                   // }
                   //t.trigger('active',[t,t.tabs[tabindex]]);
                //}
                t.trigger('active',[t,tab]);
            },
            hide: function (tabid) {
                var t = this;
                var li = $(t.renderto).find('li[toggleid="'+tabid+'"]');
                var tabindex=$(t.renderto).find('li').index(li);  
                li.remove();
                $('#'+tabid).remove();

                var tab = t.getTabIndex(tabid);
                //console.log(tabid + tab);
                //t.trigger('active',[t,t.tabs[tabindex]]);
                t.tabs.splice(tab,1);
                
                //隐藏后显示后一个，如果后一个本来就隐藏就显示再后一个
                for (var k = tab; k >= 0 ; k--) {
                    //console.log(k);
                    if (t.tabs[k] && t.tabs[k].id!='') {
                        t.show(t.tabs[k]);
                        break;
                    }
                }
                if (t.tabs && t.tabs[tabindex] && t.tabs[tabindex].id) {
                }
                t.trigger('hidden',[t]);
            },
            init: function () {//绘制方法
                var t = this;
                $(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                t.navtabs = $('<ul class="nav nav-tabs" style="margin-bottom:2px;"></ul>');
                t.tabcontent = $('<div class="tab-content"></div>');
                $(t.renderto).append(t.navtabs);
                $(t.renderto).append(t.tabcontent);
                var is_active = false;

                
                if(t.showrmenu){
                    t.on("added",function(thistab,thistabitem){
                        var items = [];
                        items.push({text:thistab.close,icon:"pencil",onClick:function(e){
                            var tid = e.attr("toggleid");
                            //var _thistabitem = thistab.getTab(tid);
                            thistab.hide(tid);
                        },
                        isEnabled:function(e){
                            //如果没有关闭不显示
                            var tid = e.attr("toggleid");
                            var _thistabitem = thistab.getTab(tid);
                            return _thistabitem.showclosebtn;}
                        });
                        items.push({divider:true});//分隔符
                        items.push({text:thistab.closeother,icon:"pencil",onClick:function(e){
                                var tid = e.attr("toggleid");
                                var ids = [];
                                //关闭可以关闭的
                                thistab.tabs.forEach(function(_tab){
                                    if(_tab.id!=tid && _tab.showclosebtn){
                                        ids.push(_tab.id);
                                    }
                                });
                                for(var i=0;i<ids.length;i++){
                                    thistab.hide(ids[i]);
                                }
                            },
                            isEnabled:function(e){
                                //如果没有可以关闭的不显示
                                var tid = e.attr("toggleid");
                                var f = false;
                                thistab.tabs.forEach(function(_tab){
                                    if(_tab.id!=tid && _tab.showclosebtn){
                                        f= true;
                                    }
                                })
                                return f;
                            }
                        });
                        items.push({text:thistab.closeall,icon:"pencil",onClick:function(e){
                                //var tid = e.attr("toggleid");
                                var ids = [];
                                //关闭可以关闭的
                                thistab.tabs.forEach(function(_tab){
                                    if(_tab.showclosebtn){
                                        ids.push(_tab.id);
                                    }
                                });
                                for(var i=0;i<ids.length;i++){
                                    thistab.hide(ids[i]);
                                }
                            },
                            isEnabled:function(e){
                                //如果没有可以关闭的不显示
                                var tid = e.attr("toggleid");
                                var f = false;
                                thistab.tabs.forEach(function(_tab){
                                    if(_tab.showclosebtn){
                                        f= true;
                                    }
                                })
                                return f;
                            }
                        });
                        new ex.RMenu({
                            renderto:'[toggleid="'+thistabitem.id+'"]',
                            items:items
                        })
                    })
                }

                $.each(t.right, function (i, _right) {
                    if(!_right.id || _right.id==''){_right.id = t.id+"_tabright"+"-"+(i+1)};
                    var r = $('<li class="pull-right"></li>');
                    var r_a = $('<a href="#" id="'+_right.id+'">'+_right.text + '</a>');
                    if(_right.children){

                    }
                    if(_right.onClick){
                        r_a.on("click",_right.onClick);
                    }
                    r.append(r_a);
                    $(t.navtabs).append(r);
                });

                $.each(t.tabs, function (i, _tab) {
                    tab = $.extend(items(), _tab);
                    if(!tab.id || tab.id==''){tab.id = t.id+"_tab"+"-"+(i+1)};
                    addtab(t,tab);
                    if (!is_active && tab.active) {
                        is_active = true;
                        $(tab.tabel).addClass("active");
                        $(tab.bodyel).addClass("active");
                    }
                    tab.doLayout(t);
                    t.tabs[i] = tab;
                    //
                })
                //$(t.renderto).find('.tab-pane').css('min-height', $(t.renderto).height());
                //绘制右键
                t.firstInit = true;
                //ex.doCallback(t.onFirstInited, [t]);
                t.trigger('inited',[t])

                $(t.renderto).on('shown.bs.tab',function(e){
                    t.trigger('active',[t,t.getactive()]);
                });
            },
            getactive:function(){//得到当前激活的页签
                var t = this;
                var index;
                t.tabs.forEach(function(v,i){
                    if(t.isactive(v)){
                        index = i;
                    }
                });
                if(index>=0){
                    return t.tabs[index];
                }
            },
            isactive: function (tab) {
                var tabindex;
                var t = this;
                tabindex = t.getTabIndex(tab);
                if(t.tabs[tabindex]){
                //return $(t.renderto).find('a[href="#'+t.tabs[tabindex].id+'"]').parent().hasClass("active");
                  return $(t.renderto).find('.nav-tabs li.active').find('a[href="#'+t.tabs[tabindex].id+'"]').length>0;
                }
            },
            addTab:function(_tab){
                var t =this;
                if(_tab.id && t.getTabIndex(_tab.id)>=0){
                    t.show(_tab.id);
                }else{
                     tab = $.extend(new items(), _tab);
                     var i = t.tabs.length;
                     //if(!tab.id || tab.id==''){tab.id = t.id+"_tab"+"-"+(i+1)}
                     if(tab.url && tab.url!=''){tab.isiframe= true;}
                     addtab(t,tab);
                     t.iframesize();
                     t.tabs.push(tab);
                     if(tab.active){
                        t.show(tab);
                     }
                }
            },
            //得到页签实例
            getTab:function(_tab){
                var t = this;
                var tabindex = t.getTabIndex(_tab);
                var tab = t.tabs[tabindex];
                tab.active = t.isactive(tab);
                return tab;
            },
            //得到页签序号
            getTabIndex:function(_tab){
                var t = this;
                var tabindex = -1;
                var tab,tabid;
                //数字就是序号
                if(ex.isNub(_tab)){
                    tab=t.tabs[_tab];
                    if(tab){
                        tabindex=_tab;
                        tabid=tab.id;
                    }
                }
                //字符串是ID
                if(ex.isStr(_tab)){
                    tab=t.tabs.find(function(item){ return item.id==_tab });
                    if(tab){
                        tabindex=t.tabs.indexOf(tab);
                        tabid=tab.id;
                    }
                }
                //对象就是对象
                if(ex.isObj(_tab)){
                    tabindex = t.tabs.indexOf(_tab);
                    if(tabindex>=0){
                        tab=t.tabs[tabindex];
                        tabid=tab.id;
                    }
                }
                //console.log(_tab+'|'+tabindex);
                return tabindex;
            }
        });
        ex.Tab = _Tab;
    })(bsEx, window);

    //Navbar 导航栏 原创
    //依赖 ex.ui.base
    (function (ex) {
        //继承公共基础属性
        var exb = new ex.ui.base();
        var _Navbar = function (op) {
            var t = this;
            //继承事件基类
            $.extend(t,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }
            return t;
        }
        var language =  ex.delanguage().Navbar;
        var menudefault = function () {
            this.id="";//ID
            this.text="";//显示文字
            this.url="";//连接地址
            this.class=[];//自定义样式
            this.children=[];//子集
            this.isactive=false;//是否激活
            this.istitle=false;//是否是标签
            this.enable=true;//是否可用
        }
        var header ="";
        _Navbar.prototype = $.extend(exb, {
            type:"Navbar",//控件类型
            title: language.title(),//标题
            meuns:[],//菜单 中间部分
            right:[],//右侧菜单
            class:"",//自定义样式
            header:"",//头元素
            meunel:"",//菜单元素
            //绘制
            init: function(){
                var t = this;
                $(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                //拼写body
                var body  = $('<nav class="navbar navbar-default"><div class="container-fluid">');
                if(t.class!=""){
                    body.addClass(t.class);
                }
                var header=$('<div class="navbar-header">');
                var headerbtn = $('<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#'+t.id+'" aria-expanded="false" aria-controls="navbar">');
                headerbtn.append('<span class="sr-only"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>');
                header.append(headerbtn); 
                header.append('<a class="navbar-brand" href="#">' + t.title + '</a>');
                body.append(header);
                t.header = header;

                //组建菜单 li
                var bindmeun = function(items,dom){
                    $.each(items, function (i, _meun) {
                        meun = $.extend(new menudefault(), _meun);
                        var mitem = $('<li>');
                        //ID
                        if(!meun.id || meun.id==''){
                            meun.id=t.id+"-item_"+(i+1);
                        }
                        mitem.attr('id',meun.id);
                        //激活
                        if(meun.isactive){mitem.addClass('active');}
                        //地址
                        if(meun.url && (!meun.target || meun.target!='_blank')){
                            mitem.append('<a href="#" data-url="'+meun.url+'">'+meun.text+'</a>');
                        }
                        else if(meun.url && meun.target && meun.target=='_blank'){
                            mitem.append('<a href="'+meun.url+'"  target="_blank">'+meun.text+'</a>');
                        }
                        //子菜单
                        else if(meun.children && meun.children.length>0){
                            mitem.addClass('dropdown');
                            mitem.append('<a data-toggle="dropdown" class="dropdown-toggle" href="#">'+meun.text+'<strong class="caret">');
                            var childrenmeun = $('<ul class="dropdown-menu">');
                            $.each(meun.children, function (j, _son) {
                                son = $.extend(new menudefault(), _son);
                                var sitem = $('<li>');
                                //ID
                                if(!son.id || son.id==''){
                                    son.id=t.id+"-item_"+i+"_"+(j+1);
                                }
                                sitem.attr('id',son.id);
                                //激活
                                if(son.isactive){
                                    mitem.addClass('active');
                                    sitem.addClass('active');
                                }
                                //标题
                                if(son.istitle){
                                    sitem.addClass('nav-header');sitem.html(son.text);
                                }
                                //连接地址
                                else if(son.url){
                                    sitem.append('<a href="#" data-url="'+son.url+'">'+son.text+'</a>');
                                }
                                else{
                                    sitem.append('<a href="#">'+son.text+'</a>');
                                }
                                childrenmeun.append(sitem);
                            });
                            mitem.append(childrenmeun);
                        }
                        else{
                            mitem.append('<a href="#">'+meun.text+'</a>');
                        }
                        dom.append(mitem);
                    });
                }

                //拼写菜单
                var meunel = $('<div id="'+t.id+'" class="navbar-collapse collapse">');
                var middle = $('<ul class="nav navbar-nav">');
                $.each(t.meuns, function (i, _meun) {
                    if(!_meun.id || _meun.id==''){
                        _meun.id=t.id+"-item_"+(i+1);
                    }
                    if(_meun.children){
                    $.each(_meun.children, function (j, _son) {
                        if(!_son.id || _son.id==''){
                            _son.id=t.id+"-item_"+i+"_"+(j+1);
                        }
                    })}
                });
                bindmeun(t.meuns,middle);
                meunel.append(middle);

                //拼写右侧菜单
                var right = $('<ul class="nav navbar-nav navbar-right">');
                $.each(t.right, function (i, _meun) {
                    if(!_meun.id || _meun.id==''){
                        _meun.id=t.id+"-item_"+(i+1);
                    }
                    if(_meun.children){
                    $.each(_meun.children, function (j, _son) {
                        if(!_son.id || _son.id==''){
                            _son.id=t.id+"-item_"+i+"_"+(j+1);
                        }
                    })}
                });
                bindmeun(t.right,right);
                meunel.append(right);

                body.append(meunel);
                t.meunel = meunel;
                $(t.renderto).append(body);

                t.firstInit = true;
                //ex.doCallback(t.onFirstInited, [t]);
                t.trigger('inited',[t])
            },
            //选择项
            selectItem:function(meun){
                var t = this;
                var m = t.getMeunitem(meun);
                if(m && m.id && m.id!=''){
                    $(t.renderto).find("li").removeClass('active');
                    //判断是否为子集 如果是子集需要选中 上级和当前
                    if($("#"+m.id).parent().hasClass("dropdown-menu")){
                        $("#"+m.id).parent().parent().addClass('active');
                    }
                    $("#"+m.id).addClass('active');
                }
            },
            //得到菜单项
            getMeunitem:function(meun){
                var t = this;
                var m;
                //console.log(meun)
                if(ex.isStr(meun)){
                   m = t.meuns.find(function(item){return item.id==meun});
                }
                if(!m){
                    t.meuns.forEach(function(item){
                        if(item.children && !m){
                           m = item.children.find(function(citem){
                               return citem.id==meun
                            });
                        }
                    })
                }
                return m;
            }
        });
        ex.Navbar = _Navbar;
    })(bsEx, window);

    //ItemForm 表单
    //依赖 ex.ui.base|jquery.smart-form表单布局|bootstrap-select下拉|bootstrap-datetimepicker日期
    (function (ex) {
        //继承公共基础属性
        var exb = new ex.ui.base();
        var _ItemForm = function (op) {
            var t = this;
            //注册事件
            $.extend(this,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }

            t.on("inited",function(thisform){
                $.each(thisform.items,function(i,item){
                    if(item && item.name && item.type!='' && initcontrol[item.type]){
                        initcontrol[item.type].init(item.name,item);
                    }
                });

                setTimeout(function(){
                    //重新布局
                    thisform.doLayout();
                }, 0);
            })
            return t;
        }
        var language =  ex.delanguage().ItemForm;
        var itemdefault = function(){
            //this.id=ex.getId();
            this.type="";//类型
            this.name="";//字段名称
            this.title="";//显示标题
            this.oneline = false;//是否单独一行
            this.validate= false;
        };
        var initcontrol={
            //时间控件
            datetime:{
                getoption:function(op){
                    var _op = {
                        type: "text",
                        next:{
                            text:'<span class="glyphicon glyphicon-th">'
                        },
                        group:{
                            className :"date form_datetime",
                            extendAttr :{"date-format": "yyyy-mm-dd"},
                            extendAttr :{"date-language": "zh-CN" }
                        }
                   }
                   if(op){
                        $.extend(op,_op);
                   }
                   return op;
                },
                init:function(id,item){
                    //限制输入
                    $('[name="'+id+'"]').keydown(function (event) {
                        return global.Fn.checkKeyForFloat($(this).val(), event);
                    });
                    var timede = {
                        weekStart: 1,
                        todayBtn: 1,
                        autoclose: 1,
                        todayHighlight: 1,
                        startView: 2,
                        minView: 2,
                        forceParse: 0,
                        format: "yyyy-mm-dd"
                    };
                    if(item.time){
                        $.extend(timede,item.time);
                    }
                    $('[name="'+id+'"]').parent().datetimepicker(timede);
                }
            },
            //多行控件
            textarea:{
                getoption:function(op){
                    if(op){
                      op.type = "text";
                    }
                    return op;
                },
                init:function(id,item){
                    var h = $('[name="'+id+'"]').parent().html();
                    h = h.replace("<input ", "<textarea ");
                    $('[name="'+id+'"]').parent().html(h);
                }
            },
            //数字控件
            numbertxt:{
                getoption:function(op){
                    if(op){
                        op.type = "text";
                    }
                    return op;
                },
                init:function(id,item){
                    //限制输入
                    $('[name="'+id+'"]').keydown(function (event) {
                        return global.Fn.checkKeyForInt($(this).val(), event);
                    });
                }
            },
            //浮点控件
            floattxt:{
                getoption:function(op){
                   if(op){
                     op.type = "text";
                   }
                   return op;
                },
                init:function(id,item){
                   //限制输入
                   $('[name="'+id+'"]').keydown(function (event) {
                        return global.Fn.checkKeyForFloat($(this).val(), event);
                    });
                }
            },
            //下拉控件
            select:{
                getoption:function(op){
                    var _op = {
                        select: { },
                        items:[]
                    }
                    $.extend(_op,op);
                    return _op;

                },
                init:function(id,item){
                    var _s = {
                        id : item.name,
                        renderto: ('[name="'+item.name+'"]'),
                        data:item.items
                    }
                    $.extend(_s,item.select)
                    s =  new ex.Dropdownlist(_s); 
                    if(s){
                        s.init();
                    }
                }
            }
        }
        var GetTitleByname = function (items, name) {
            var t = "";
            $.each(items, function (i, v) {
                if (v.name == name) {
                    t = v.title;
                }
            });
            return t;
        };
        var GetItemByname = function (items, name) {
            var t = {};
            $.each(items, function (i, v) {
                if (v.name == name) {
                    t = v;
                }
            });
            return t;
        };
        _ItemForm.prototype = $.extend(exb, {
            type:"ItemForm",//控件类型
            items:[],//表单绘制项
            readonly:[],//只读的字段
            requireds:[],//要验证的字段
            validate: false,
            colunb: 1,//绘制列数
            BSForm: {},//BSForm 底层核心插件
            BSFormop: {},//BSForm 的扩展项
            valmsg:language.valmsg(),//请填写
            autoLayout: ['1,11', '1,5', '1,3', '1,2', '1,2,1,2,1,2,1,2'],//布局 根据 列数 对应数组中的值
            depopover:{},//弹出提示配置
            init:function(){
                var t = this;
                if(!t.renderto || t.renderto==""){
                    t.renderto = $('<form action="" id="'+ t.id +'"></form>');
                }
                if( $(t.renderto)[0].tagName!='FORM'){
                    $(t.renderto).append('<form action="" id="'+ t.id +'"></form>');
                    t.renderto = t.renderto.find("form");
                }
                $(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                /*根据参数 绘制BSForm 底层核心插件
                BSForm 的核心参数有；
                eles        字段集合
                hides       隐藏的字段集合
                autoLayout  布局信息
                绘制方法为；
                    new BSForm({ eles: _eles, hides: _hides, autoLayout: _autoLayout }).Render('id');
                */
                //得到表单(jquery.smart-form)需要的配置项
                var BSFormdefault = function(){
                    var _eles = [], _hides = [];
                    var _autoLayout = t.autoLayout[t.colunb - 1];//正常列 隐藏列 布局
                    //绘制 _eles 和 _hides
                    var formitem =  JSON.parse(JSON.stringify(t.items));//项 
                    var colunb = t.colunb;//列数
                    if ($.isArray(formitem)) {
                        var rownub = Math.ceil(formitem.length / colunb);//行数
                        var index = 0;//item计数
                        //循环行 绘制项
                        for (var j = 0; j < rownub; j++) {
                            var _e = [];//本地对象
                            //循环该行列
                            for (var i = 0; i < parseInt(colunb); i++) {
                                var thisitem = formitem[index];//当前项
                                thisitem =  $.extend(new itemdefault(),thisitem);
                                if (thisitem && thisitem.type!="") {
                                    var itemtype = thisitem.type;
                                    if (itemtype != 'hides') {
                                        var BSitem = {};
                                        if (itemtype == "text") {
                                            BSitem = thisitem;
                                        } else {
                                            BSitem = initcontrol[itemtype].getoption(thisitem);//得到绘制方法
                                        }
                                        _e.push({ ele: BSitem });
                                    }
                                    else{
                                        var _d = {};
                                        $.each(thisitem, function (n, m) {
                                            if (n != "type") {
                                                _d[n] = m;
                                            }
                                        })
                                        _hides.push(_d);
                                    }
                                }
                                index++;
                            }
                            //循环列结束
                            if (_e.length != 0) {
                                _eles.push(_e)
                            };
                            if (index == formitem.length) {
                                break;
                            }
                        }
                    }
                    return { eles: _eles, hides: _hides, autoLayout: _autoLayout };
                }
                var _BSFormop =BSFormdefault();
                $.extend(_BSFormop, t.BSFormop);
                var id =  $(t.renderto).attr('id');
                if(!id || id==""){
                    id = t.id;
                    $(t.renderto).attr('id',id)
                }
                t.BSForm = new BSForm(_BSFormop).Render(id);

                
                if (t.validate) {
                    var validatemessages = {};
                    var validaterules = {};
                    t.__frompopover = [];
                    $.each(t.requireds, function (i, v) {
                        validaterules[v] = {};
                        validaterules[v].required = true;
                        var item = GetItemByname(t.items, v);
                        validatemessages[v] = {};
                        validatemessages[v].required =t.valmsg + GetTitleByname(t.items, v);
                        if (item.validate) {
                            if (item.validate.rules) {
                                validaterules[v] = $.extend(validaterules[v], item.validate.rules);
                            }
                            if (item.validate.messages) {
                                validatemessages[v] = $.extend(validatemessages[v], item.validate.messages);
                            }
                        }
                    })
                    $(t.renderto).validate({
                        rules: validaterules,
                        messages: validatemessages,
                        onfocusout: function () {
                            $.each(t.__frompopover, function (i, v) {
                                //var t = $('#' + v).parents("div[class^='col-sm']");
                                var _t = $('#' + v);
                                //$(_t).popover('hide');
                                $(_t).attr("data-content", "");
                                $(_t).popover("show");
                            });
                            t.__frompopover = [];
                            setTimeout(function () {
                                $(t.renderto).valid();
                            }, 400)
                        },
                        showErrors: function (em, eList) {
                            //每次触发把以前的删掉
                            $.each(em, function (i, v) {
                                var _t = $('#' + i);
                                if (t.__frompopover.indexOf(i) < 0) {
                                    t.__frompopover.push(i);
                                }
                                if ($(_t)) {
                                    $(_t).attr("data-toggle", "popover");
                                    var popover = $.extend({
                                        trigger: "hover",
                                        delay: { show: 100, hide: 5000 },
                                        placement: "top"
                                    },t.depopover)
                                    $(_t).popover(popover);
                                    $(_t).attr("data-content", v);
                                    $(_t).popover("show");
                                    setTimeout(function () { $(_t).popover("hide"); }, 3000);
                                }
                            });
                        }
                    });
                }

                t.firstInit = true;
                //ex.doCallback(t.onFirstInited, [t]);
                t.trigger('inited',[t]);
            },
            //验证
            valid: function () { 
                var t = this;
                return $(t.renderto).valid(); 
            },
            //重新布局 设置只读状态
            doLayout: function () {
                var t = this;
                //整体布局
                //$(t.renderto).find('.form-group').find('div').eq(0).css('margin-bottom','0px');
                $(t.renderto).find('.form-group').prepend('<div class="row" style="margin-bottom:10px"></div>');
                $(t.renderto).find('.form-group').css('margin-bottom', '10px');
                $(t.renderto).find('.control-label').css('line-height', $(t.renderto).find('.form-control').eq(0).outerHeight() + 'px');

                //得到浏览器参数 view 如果为true 则为全部只读
                var view = ex.getparastr("view") === "true" || false;
                //每个项布局
                $.each(t.items, function (i, v) {

                    //如果该项只读 则加入到只读集合
                    if (v.readonly) {
                        if ($.isArray(t.readonly)) {
                            t.readonly.push(v.name);
                        }
                    }
                    //判断是否只读
                    if (view || t.readonly == true || ($.isArray(t.readonly) && t.readonly.indexOf(v.name) >= 0)) {
                        // select 控件只读设置
                        if (v.type && v.type.indexOf("select") >= 0) {
                            var s = selectItem("#" + v.name);
                            s.disabled = true;
                            s.setdisabled();
                            setTimeout(function () {
                                $(t.renderto).find("[data-toggle='dropdown'][data-id='" + v.name + "']").addClass("disabled");
                            }, 600);
                        }
                        //日期控件 强制隐藏 
                        else if (v.type && v.type.indexOf("datetime") >= 0) {
                            $('[name="'+v.name+'"]').parent().datetimepicker('remove');
                            $('[name="'+v.name+'"]').attr("readonly", "");
                        } else {
                            $('[name="'+v.name+'"]').attr("readonly", "");
                        }

                        //删除验证 
                        t.requireds.remove(v.name);
                    }
                    //判断该项 强制在一行 一般是最后一行
                    if (v.oneline) {
                        //重置 控件class
                        var classnames = $('[name="'+v.name+'"]').parent().attr("class").split(' ');
                        $.each(classnames, function (a, b) {
                            if (b.indexOf('col-sm') >= 0) {
                                classnames[a] = 'col-sm-11';
                            }
                        })
                        $('[name="'+v.name+'"]').parent().prev().before('<div class="row" style="margin-bottom: 10px;"></div>');
                        $('[name="'+v.name+'"]').parent().attr("class", classnames.join(' '));
                        //.addClass("col-sm-11");

                        //重置 lable class
                        var lable_classnames = $('[name="'+v.name+'"]').parent().prev().attr("class").split(' ');
                        $.each(lable_classnames, function (a, b) {
                            if (b.indexOf('col-sm') >= 0) {
                                lable_classnames[a] = 'col-sm-1';
                            }
                        })
                        $('[name="'+v.name+'"]').parent().prev().attr("class", lable_classnames.join(' '));

                        //执行换行
                        if (!$('[name="'+v.name+'"]').parent().next().hasClass('row')) {
                            $('[name="'+v.name+'"]').parent().next().before('<div class="row" style="margin-bottom: 10px;"></div>');
                        }
                    }
                    //判断该项后 强制换行
                    if (v.enline) {
                        $('[name="'+v.name+'"]').parent().prev().before('<div class="row" style="margin-bottom: 10px;"></div>');
                    }
                })
            },
            //得到值
            getFormData: function () {
                var d = this.BSForm.GetFormData();
                var t = this;
                $.each(t.items, function (i, v) {
                    //select控件 把选择的中文也赋上 key为str_加name 
                    if (v.type && v.type.indexOf('select') >= 0) {
                        if ($(t.renderto).find("[data-toggle='dropdown'][data-id='" + v.name + "']").length > 0 && $(t.renderto).find("[data-toggle='dropdown'][data-id='" + v.name + "']").attr("title").indexOf('请选择') <= 0) {
                            d["str_" + v.name] = $(t.renderto).find("[data-toggle='dropdown'][data-id='" + v.name + "']").attr("title");
                        }
                    }
                })
                return d;
            },
            //设置值
            setFormData: function (model) {
                return this.BSForm.InitFormData(model);
            }
        });
        ex.ItemForm = _ItemForm;
    })(bsEx, window);

    //dropdownlist 下拉框
    //依赖 ex.ui.base|bootstrap-select下拉
    (function (ex) {
        //继承公共基础属性
        var exb = new ex.ui.base();
        var _dropdownlist = function (op) {
            var t = this;
            //注册事件
            $.extend(this,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                //t.init();
            }
            return t;
        }
        var language =  ex.delanguage().Dropdownlist;
        //重置树data
        var GetTreeData = function (_data, parentfield, idfield) {
            var onelevel = [];//第一级
            var _level = 0;
            for (var i = 0; i <= _data.length; i++) {
                var d = _data[i];
                if (d) {
                    var pid = d[parentfield];
                    var n = true;
                    for (var k = 0; k <= _data.length; k++) {
                        var _d = _data[k];
                        if (_d && _d[idfield] && pid && pid == _d[idfield]) {
                            n = false;
                            break;
                        }
                    }
                    if (n) {
                        d._level = _level.toString();
                        onelevel.push(d);
                    }
                }
            }
            var treedata = [];
            for (var i = 0; i <= onelevel.length; i++) {
                treedata.push(onelevel[i]);
                var SonData = GetSonData(_data, onelevel[i], parentfield, idfield, 1);
                if (SonData) {
                    $.each(SonData, function (n, m) {
                        treedata.push(m);
                    })
                }
            }
            return treedata;
        }
        //得到子集数据
        var GetSonData = function (AllData, onedata, parentfield, idfield, _level) {
            var SonData = [];
            _level++;
            for (var i = 0; i <= AllData.length; i++) {
                var d = AllData[i];
                if (d && onedata && d[parentfield] == onedata[idfield] && d[idfield] != onedata[idfield]) {
                    d._level = _level.toString();
                    if (!onedata.allparents) {
                        onedata.allparents = [];
                    }
                    if (!d.allparents) {
                        d.allparents = [];
                    }
                    d.allparents = onedata.allparents.concat();
                    d.allparents.push({ _level: onedata._level == 0 ? 1 : onedata._level, id: onedata.id });
                    SonData.push(d);
                    var _s = GetSonData(AllData, d, parentfield, idfield, _level);
                    if (_s) {
                        $.each(_s, function (n, m) {
                            if (m) {
                                SonData.push(m);
                            }
                        })
                    }
                }
            }
            return SonData;
        }
        _dropdownlist.prototype = $.extend(exb, {
            type:"Dropdownlist",//控件类型
            url: "",//数据地址
            queryData: {},//查询传入的数据
            data: [],//数据
            treedata: [],
            idfield: "id",//ID列名
            textfield: "name",//显示列名
            parentfield: "pId",//父ID列名
            tree: false,//是否启用tree
            treelevel: false,//是否自动勾选子集
            firstInit: true,
            disabled: false,//是否禁用
            multiple: false,//是否多选
            initval: [],//初始值
            search: true,//是否显示搜索
            showclear: true,//是否显示清空按钮
            noneSelectedText:language.selectnoneText(),
            deselectAllText:language.deselectAllText(),
            selectAllText:language.selectAllText(),
            onselectchangeddone: false,//是否已经执行change事件
            lastselect: [],//上一次勾选
            refresh: function () {},//刷新方法
            optdisabledfn: function (d) { return false; },//如果改方法返回true 该项将被 disabled
            removedatafn: function (d) { return false; },//如果该方法返回true 该项将被删除
            //绑定方法 绘制
            init: function (_data) {
                var t = this;
                $(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                if (_data) {
                    t.data = _data;
                }
                if (t.firstInit && t.url) {
                    jQuery.support.cors = true;
                    var ajaxop = {
                        url: t.url,
                        type: "POST",
                        async: true
                    };
                    if (JSON.stringify(t.queryData) != '{}') {
                        ajaxop.data = t.queryData;
                    }
                    var _ajax1 = $.ajax(ajaxop);
                    $.when(_ajax1).done(function (responseText) {
                        var _responseText = GetAjaxArray(responseText);
                        t.firstInit = false;
                        t.init(_responseText);
                    }).fail(function () {
                    });
                } else {
                    if (t.tree) {
                        t.data = GetTreeData(t.data, t.parentfield, t.idfield);
                    }
                    $(t.renderto).html('');
                    var _renderto = t.renderto;
                    //class="selectpicker show-tick form-control" multiple data-live-search="true">
                    if (!$(t.renderto).hasClass('selectpicker')) {
                        $(t.renderto).addClass('selectpicker');
                    } if (!$(t.renderto).hasClass('show-tick')) {
                        $(t.renderto).addClass('show-tick');
                    } if (!$(t.renderto).hasClass('form-control')) {
                        $(t.renderto).addClass('form-control');
                    }
                    /* if (t.multiple) {*/
                    $(t.renderto).attr('multiple', "true");

                    /*}
                    else {
                        $(t.renderto).removeAttr('multiple');
                    }*/
                    if (t.search) {
                        $(t.renderto).attr('data-live-search', t.search);
                    }
                    else {
                        $(t.renderto).attr('data-live-search', 'false');
                    }
                    if (!t.data) {
                        t.data = [];
                    }
                    $.each(t.data, function (i, v) {
                        if (v) {
                            //data-content="<i class=\'f-tree-cell-icon\'></i><span class=\'text\'>'+ v.text +'</span>"
                            var item = '<option value="' + v[t.idfield] + '" title="' + v[t.textfield] + '" data-content>' + v[t.textfield] + '</option>';
                            var level = '';
                            for (var k = 1; k < parseInt(v._level); k++) {
                                level += '<i class=\'f-tree-cell-icon\'></i>';
                            }
                            item = item.replace("data-content", 'data-content="' + level + '<span class=\'text\'>' + v[t.textfield] + '</span>"');
                            if (t.optdisabledfn(v)) {
                                item = item.replace("<option ", "<option disabled ")
                            }
                            if (!t.removedatafn(v)) {
                                $(_renderto).append(item);
                            }
                        }
                    })
                    //maxOptions
                    var selectopt = { noneSelectedText: t.noneSelectedText, actionsBox: true, deselectAllText: t.deselectAllText, selectAllText: t.selectAllText };
                    //不为多选时 设置为多选并且最大选择项为1 保持单选
                    if (!t.multiple) {
                        selectopt.maxOptions = 1;
                        // selectopt.actionsBox=false;
                    }
                    if (t.disabled) {
                        selectopt.disabled = true;
                    }
                    $(t.renderto).selectpicker(selectopt);
                    $(t.renderto).selectpicker('refresh');
                    //bs-select-all
                    if (!t.multiple) {
                        $(t.renderto).prev().find('.bs-select-all').hide();
                        $(t.renderto).prev().find('.bs-deselect-all').css('width', '100%');
                        $(t.renderto).prev().find('.bs-deselect-all').css('border-radius', '3px');
                    }
                    if (!t.showclear) {
                        $(t.renderto).prev().find('.bs-deselect-all').hide();
                    }
                    if (t.initval.lenght != 0) { $(t.renderto).selectpicker('val', t.initval); }
                    t.firstInit = false;
                }

                $(t.renderto).on('changed.bs.select', function (e) {
                    var v = [];
                    if (typeof ($(t.renderto).val()) == "string") {
                        v = $(t.renderto).val().toString().split(',');
                    }
                    else if (typeof ($(t.renderto).val()) == "array") {
                        v = $(t.renderto).val();
                    } else if (typeof ($(t.renderto).val()) == "object") {
                        v = eval($(t.renderto).val());
                    }
                    if (!v) { v = [] }
                    var changeitem = Array.minus(v, t.lastselect);
                    if (changeitem.length != 0) {
                        t.onselectchangeddone = false;
                    }
                    if (t.tree && t.treelevel && t.multiple) {
                        $.each(changeitem, function (k, c) {
                            //得到c的子集
                            var c_data = {};
                            $.each(t.data, function (i, _v) {
                                if (_v && _v[t.idfield] == c) {
                                    c_data = _v;
                                }
                            });
                            var SonData = GetSonData(t.data, c_data, t.parentfield, t.idfield, c_data._level);

                            if (v.indexOf(c) >= 0) {//存在就是新勾选的
                                if (SonData) {
                                    $.each(SonData, function (i, _v) {
                                        if (_v) {
                                            v.push(_v[t.idfield]);
                                        }
                                    });
                                }
                            } else {//不存在就是取消勾选
                                if (SonData) {
                                    $.each(SonData, function (i, _v) {
                                        if (_v) {
                                            v.remove(_v[t.idfield]);
                                        }
                                    });
                                }
                            }
                        })
                    }
                    t.lastselect = v;
                    t.selectpicker('val', v);
                    if (!t.onselectchangeddone) {
                        //t.onselectchanged(v);
                        //eval("var fun;try{fun=" + t.renderto.replace("#", "").replace(".", "") + "_onselectchanged;}catch(ex){}");
                        //if (fun) {
                        //    fun(v);
                        //}
                        t.trigger("selectchanged",[v]);
                        t.onselectchangeddone = true;
                    } else { }
                });
                t.trigger('inited',[t]);
            },
            //设置方法
            setvalue: function (vals) {
                var t = this;
                $(this.renderto).selectpicker('val', vals);
                //t.onselectchanged(vals);
                //eval("var fun;tt.onselectchangedry{fun=" + t.renderto.replace("#", "").replace(".", "") + "_onselectchanged;}catch(ex){}");
                //if (fun) {
                //    fun(vals);
                //}
                t.trigger("selectchanged",[vals]);
                t.onselectchangeddone = true;
            },
            //得到选择值
            getval: function () {
                var v = [];
                var t = this;
                if (typeof ($(t.renderto).val()) == "string") {
                    v = $(t.renderto).val().toString().split(',');
                }
                else if (typeof ($(t.renderto).val()) == "array") {
                    v = $(t.renderto).val();
                } else if (typeof ($(t.renderto).val()) == "object") {
                    v = eval($(t.renderto).val());
                }
                if (!v) { v = [] }
                return v;
            },
            //得到选择项的data
            getvaldata: function (v) {
                var t = this;
                var val = {};
                if (!v) {
                    v = t.getval();
                    if (!v || v.length <= 0) {
                        return val;
                    }
                }
                var index = t.data.findIndex(function (e) { return e[t.idfield] == v; });
                if (index < 0) {
                } else {
                    val = t.data[index];
                }
                return val;
            },
            //选择完成后事件
            onselectchanged: function (v) { },
            selectpicker: function (k, v) {
               return $(this.renderto).selectpicker(k, v);
            },
            setdisabled: function (b) {
                var t = this;
                if (b) {
                    setTimeout(function () {
                        $("[data-toggle='dropdown'][data-id='" + t.renderto.replace('#', '') + "']").addClass("disabled");
                    }, 600);
                } else {
                    setTimeout(function () {
                        $("[data-toggle='dropdown'][data-id='" + t.renderto.replace('#', '') + "']").removeClass("disabled");
                    }, 600);
                }
            }
        });
        ex.Dropdownlist = _dropdownlist;
    })(bsEx, window);

    //panel 面板
    //依赖 ex.ui.base
    (function (ex) {
        //继承公共基础属性
        var exb = new ex.ui.base();
        var _Panel = function (op) {
            var t = this;
            //继承事件基类
            $.extend(this,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }
            return t;
        }
        var language =  ex.delanguage().Panel;
        _Panel.prototype = $.extend(exb, {
            type:"Panel",//控件类型
            showhead:true,//是否显示头
            showtitle:true,//是否显示标题
            showfooter:true,//是否显示页脚
            title:language.title(),//默认标题
            headcls:"",//自定义头样式
            bodycls:"",//自定义body样式
            footercls:"",//自定义脚样式
            headel:null,//头对象
            bodyel:null,//body对象
            body:null,//body填充
            footerel:null,//脚对象
            encollapse:false,//是否禁用展开
            expanded:true,//展开状态
            btns:[],//按钮
            width:null,//宽度
            height:null,//高度
            init:function(){//绘制
                var t = this;
                $(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                $(t.renderto).addClass("panel panel-default");
                if(t.showhead){
                    var headel = $('<div class="panel-heading"></div>');
                    if(t.headcls){
                        headel.addClass(t.headcls);
                    }
                    var title = $('<div class="panel-title"></div>');
                    if(!t.encollapse){
                        var title_a = $('<a href="#'+t.id+'-panel" class="panel-toggle" data-toggle="collapse">'+ t.title +'</a>');
                        $(title).append(title_a);
                    }else{
                        $(title).append(t.title);
                    }
                    $(headel).append(title);
                    $(t.renderto).append(headel);
                    t.headel = $(headel);
                }
                var bodyel = $('<div id="'+t.id+'-panel" class="collapse in"><div class="panel-body"></div></div>');
                if(t.bodycls){
                    bodyel.addClass(t.bodycls);
                }
                $(t.renderto).append(bodyel);
                t.bodyel = $(bodyel).find(".panel-body");
                if(t.body){
                    if(ex.isObj(t.body) && t.body.init){
                        t.body.renderto= $(t.bodyel);
                        t.body.init();
                        //t.bodyel.append(t.body.renderto);
                    }else{
                        t.bodyel.append(t.body)
                    }
                }
                if(t.showfooter){
                    var footerel = $('<div class="panel-footer">');
                    if(t.footercls){
                        footerel.addClass(t.footercls);
                    }
                    if(t.btns){
                        t.btns.forEach(function(btn){ 
                            var _b = new ex.Button(btn);
                            footerel.append($(_b.renderto));
                            footerel.append("&nbsp;&nbsp;");
                        })
                    }
                    $(t.renderto).append(footerel);
                    t.footerel = $(footerel);
                }
                
                if(t.width){t.setwidth(t.width);}
                if(t.height){t.setheigth(t.height);}

                t.trigger('inited',[t]);

                //事件注册
                $(t.renderto).on("show.bs.collapse",function(){
                    t.trigger('beforeexpand',[t]);
                });
                $(t.renderto).on("shown.bs.collapse",function(){
                    t.expanded = true;
                    t.trigger('expand',[t]);
                });
                $(t.renderto).on("hide.bs.collapse",function(){
                    t.trigger('beforecollapse',[t]);
                });
                $(t.renderto).on("hidden.bs.collapse",function(){
                    t.expanded = false;
                    t.trigger('collapse',[t]);
                });

                return t;
            },
            collapse:function(){//收起
                var t = this;
                //$(t.renderto).collapse('hide');
                $(t.bodyel).parent().collapse('hide');
                
            },
            expand:function(){//展开
                var t = this;
                //$(t.renderto).collapse('show');
                $(t.bodyel).parent().collapse('show');
            },
            setwidth:function(w){//宽度
                var t = this;
                $(t.renderto).width(w);
            },
            setheigth:function(h){//高度
                var t = this;
                $(t.renderto).height(h);
                var _h = $(t.renderto).height() - 1;
                if(t.showhead){
                    _h = _h - $(t.headel)[0].offsetHeight;
                }
                if(t.showfooter){
                    _h = _h - $(t.footerel)[0].offsetHeight;
                }
                $(t.bodyel)[0].style.height = _h+"px";
                //console.log( $(t.bodyel).height() + " = " + $(t.renderto)[0].offsetHeight +"-"+$(t.headel)[0].offsetHeight+"-"+$(t.footerel)[0].offsetHeight )
                $(t.renderto).height("auto");
            }
            
        });
        ex.Panel = _Panel;
    })(bsEx, window);

    //button 按钮
    //依赖 ex.ui.base
    (function (ex) {
        //继承公共基础属性
        var exb = new ex.ui.base();
        var _Button = function (op) {
            var t = this;
            //继承事件基类
            $.extend(this,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }
            return t;
        }
        var btntype = ["default","primary","success","info","warning","danger","link"];
        var sizetype = ["lg","sm","xs"];
        var language =  ex.delanguage().Button;
        _Button.prototype = $.extend(exb, {
            type:"Button",//控件类型
            btntype:"default",//按钮样式 default|primary|success|info|warning|danger|link
            sizetype:false,//按钮大小 lg|sm|xs
            isblock:false,//block样式
            isactive:false,//是否激活
            isdisabled:false,//是否禁用
            icon:"",//图标样式
            text:language.text(),
            loadingtxt:language.text(),
            init:function(){//绘制
                var t = this;
                if(!t.renderto || t.renderto==""){
                    t.renderto = $('<button id="'+ t.id +'"></button>');
                }
                //$(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                t.attr("type","button");
                if(ex.isNub(t.btntype)){
                    t.btntype = btntype[t.btntype];
                }
                $(t.renderto).addClass("btn btn-"+t.btntype);
                if(ex.isNub(t.sizetype)){
                    t.sizetype = sizetype[t.sizetype];
                }
                if(t.sizetype){
                    $(t.renderto).addClass("btn-"+t.sizetype);
                }if(t.isblock){
                    $(t.renderto).addClass("btn-block");
                }
                t.setactive(t.isactive);
                t.setdisabled(t.isdisabled);
                if(t.icon && t.icon!=""){
                    t.seticon(t.icon);
                }
                if(t.text){
                    $(t.renderto).append('<span class="btntext">'+t.text+'</span>');
                }
                t.trigger('inited',[t]);

                $(t.renderto).on('click',function(){
                    t.trigger('click',[t]);
                })
                return t;
            },
            button:function(){//button对象
                var t = this;
                //$(t.renderto).button(arguments[0]);
                var f = $(t.renderto).button;
                return f.apply($(t.renderto),arguments);
            },
            settext:function(val){//显示文本
                var t = this;
                $(t.renderto).txt(val);
            },
            setdisabled:function(b){//是否可选
                var t = this;
                if(b){
                    $(t.renderto).addClass("disabled");
                }else{
                    $(t.renderto).removeClass("disabled");
                }
            },
            setactive:function(b){//是否激活 按下
                var t = this;
                if(b){
                    $(t.renderto).addClass("active");
                }else{
                    $(t.renderto).removeClass("active");
                }
            },
            seticon:function(icon){//设置图标
                var t = this;
                t.icon = icon;
                $(t.renderto).find(".glyphicon").remove();
                $(t.renderto).prepend('<span class="glyphicon glyphicon-'+t.icon+'"></span>'); 
            }
        });
        ex.Button = _Button;
    })(bsEx, window);

    //btn-group 按钮组
    //依赖 ex.ui.base
    (function (ex) {
        var exb = new ex.ui.base();
        var _BtnGroup = function(op) {
            var t = this;
            //继承事件基类
            $.extend(this,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }
            return t;
        }
        var language =  ex.delanguage().BtnGroup;
        _BtnGroup.prototype = $.extend(exb, {
            btns:[],//按钮组配置信息
            buttons:[],//按钮的实例
            type:"BtnGroup",
            init:function(){//绘制
                var t = this;
                //$(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                $(t.renderto).addClass("btn-group");
                t.btns.forEach(function(btn){
                    var btnel = new ex.Button(btn);
                    t.buttons.push(btnel);
                    $(t.renderto).append($(btnel.renderto));
                })
                t.trigger('inited',[t]);
            },
            showtext:function(b){//是否显示文字
                var t = this;
                if(b){
                    $(t.renderto).find('.btntext').txt('');
                }else{
                    t.buttons.forEach(function(btn){
                        btn.settext(btn.txt);
                    });
                }
            },
            insertbtn:function(index,btn){//插入按钮

            },
            appendbtn:function(){//新增按钮

            }
        });
        ex.BtnGroup = _BtnGroup;
    })(bsEx, window);


    //Grid 表格
    //依赖 ex.ui.base|bootstrap-table
    (function (ex) {
        var exb = new ex.ui.base();
        var _Griddef = function(t){
            var opt = {
                method: 'get', //请求方式（*）
                striped: true, //是否显示行间隔色
                cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true, //是否显示分页（*）
                sortable: true, //是否启用排序
                sortOrder: "asc", //排序方式
                sidePagination: "client",//分页方式：client客户端分页，server服务端分页（*）
                pageNumber: 1, //初始化加载第一页，默认第一页
                pageSize: 10, //每页的记录行数（*）
                pageList: [10, 25, 50, 100], //可供选择的每页的行数（*）
                search: false, //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: true,
                showColumns: false, //是否显示所有的列
                showRefresh: false, //是否显示刷新按钮
                minimumCountColumns: 3, //最少允许的列数
                clickToSelect: true, //是否启用点击选中行
                uniqueId: "id", //每一行的唯一标识，一般为主键列
                showToggle: false, //是否显示详细视图和列表视图的切换按钮
                cardView: false, //是否显示详细视图
                detailView: false, //是否显示父子表
                singleSelect: false,//禁止多选(不管用)
                maintainSelected: true//分页保存选择项
                //,toolbarAlign:'right'
            }
            return opt;
        }
        var _Grid = function(op) {
            var t = this;
            //继承事件基类
            $.extend(this,new ex.ui.base.eve());
            
            //this.on = function(name,fun){
            //    var t = this;
            //    $(t.renderto).on(name+'.bs.table', fun);
            //};
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                var dfop = new _Griddef(op.renderto);
                $.extend(dfop, op);
                $.extend(t, dfop);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }
            $.fn.bootstrapTable.methods.forEach(function(method){
                if(!t[method]){
                    t[method] = function(){ 
                         if(arguments[0]){
                             return t.bootstrapTable(method,arguments[0]); 
                         }else{
                             return t.bootstrapTable(method);
                         }
                    }
                }
            })

            $(t.renderto).on('all.bs.table', function(e,name,args){
                t.trigger(name.replace('.bs.table','').replace('-',''),args);
            });
            return t;
        }
        var language =  ex.delanguage().Grid;
        _Grid.prototype = $.extend(exb, {
            type:"Grid",
            dataID:undefined,//主键ID
            init:function(){//绘制
                var t = this;
                t.trigger('beforeinit',[t]);
                if(ex.isObj(t.columns)){
                    var _columns = [];
                    for (var n in t.columns) {
                        var m = t.columns[n];
                        if (n == "checkbox") {
                            _columns.push({ "checkbox": m, "field": "_selectid" });
                        }
                        else if (n == "Number" && m) {
                            _columns.push({ "field": "rowNumber", "title": m, "align": "center", "formatter": function (value, row, index) { return index + 1; } });
                        }
                        else {
                            _columns.push({ "field": n, "title": m, "align": "center" });
                        }
                    }
                    t.columns = _columns;
                }
                if(t.dataID && !t.uniqueId){
                    t.uniqueId = t.dataID;
                }
                if(t.tools){
                    var tool
                    if(!t.toolbar){
                        tool = $('<div id="' + t.id + '-toolbar" class="btn-group"></div>');
                        $(t.renderto).before(tool);
                        t.toolbar = '#'+ t.id + '-toolbar';
                    }
                    new bsEx.BtnGroup({
                        renderto:t.toolbar,
                        btns:t.tools
                    });
                }
                t.onResetView = function(){
                    t.doLayout();
                }
                t.bootstrapTable(t);
                t.trigger('inited',[t]);
                var setpageitem = function(){
                    var pagefirst = $('<li class="page-item page-first"><a class="page-link" href="#">‹‹</a></li>');
                    var pageend = $('<li class="page-item page-end"><a class="page-link" href="#">››</a></li>');
                    $(t.renderto).parents(".bootstrap-table").find("ul.pagination").prepend(pagefirst);
                    $(t.renderto).parents(".bootstrap-table").find("ul.pagination").append(pageend);
                    pagefirst.off("click").on("click",function(){
                        t.selectPage(1);
                    })
                    pageend.off("click").on("click",function(){
                        t.selectPage(t.getOptions().totalPages);
                    })
                }
                t.on("pagechange",function(){
                    setpageitem();
                    //$(t.renderto).parents(".bootstrap-table").find("ul.pagination").prepend('<li class="page-item"><a class="page-link" href="#">‹‹</a></li>');
                })
                setpageitem();
                //<li class="page-item"><a class="page-link" href="#">‹‹</a></li>
            },
            bootstrapTable:function(){//返回bootstrapTable方法
                var t = this;
                var f = $(t.renderto).bootstrapTable;
                return f.apply($(t.renderto),arguments);
            },
            getselectids:function(_field){//得到勾选的ID
                var t = this;
                var field = _field||t.dataID;
                var data = t.getselect();
                var ids = [];
                data.forEach(function(m){
                    ids.push(m[field]);
                })
                return ids;
            },
            getselect:function(){//得到勾选行
                var t = this;
                return t.bootstrapTable("getSelections");
            },
            doLayout:function(){//布局
                var t = this;
                var getHeight = function () {
                    var h = $(window).height() - $(t.renderto).offset().top;
                    if (h < 500) { h = 500 };
                    //console.log(h)
                    return h;
                };
                setTimeout(function(){
                    var h = $(t.renderto).parents(".bootstrap-table").height();
                    var _h = getHeight();
                    console.log(h + "|" + _h)
                    if(h!=_h){
                        t.bootstrapTable('resetView', {
                            height: _h
                        });
                    }
                },100)
            }
        });
        ex.Grid = _Grid;
    })(bsEx, window);

    //RMenu 右键菜单
    //依赖 ex.ui.base|BootstrapMenu
    (function (ex) {
        var exb = new ex.ui.base();
        var _RMenu = function(op) {
            var t = this;
            //继承事件基类
            $.extend(this,new ex.ui.base.eve());
            //覆盖语言包
            $.extend(t,ex.getUIlanguage(this.type));
            if (ex.isObj(op)) {
                $.extend(t, op);
            } else if(ex.isStr(op)){
                t.renderto = op;
            }
            t.firstInit = false;
            if (ex.isObj(op) && !t.firstInit) {
                t.init();
            }
            return t;
        }
        var language =  ex.delanguage().Menu;
        _RMenu.prototype = $.extend(exb, {
            items:[],//按钮组
            buttons:[],
            type:"Menu",
            rmenu:null,
            actions:null,
            actionsGroups:[],
            init:function(){//绘制
                var t = this;
                //$(t.renderto).html('');
                t.trigger('beforeinit',[t]);
                $(t.renderto).addClass("btn-group");
                if(!t.actions){
                    t.actions={};
                    t.items.forEach(function(btn){
                        //是否是分隔符
                        if(!btn.divider){
                            if(!btn.id || btn.id==""){btn.id = ex.getId();}
                            t.actions[btn.id] = {
                                name:btn.text,//显示名称
                                onClick:btn.onClick,//点击事件
                                isEnabled:btn.isEnabled//是否有效
                            };
                            if(btn.icon){t.actions[btn.id].iconClass="fa-"+btn.icon}//图标
                        }
                    })
                    var btns = [];
                    for(var i=0;i<t.items.length;i++){
                        var btn = t.items[i];
                        btns.push(btn.id);
                        if(btn.divider || t.items.length-1==i){
                            t.actionsGroups.push(btns);
                            btns=[];
                        }
                    }
                }
                try{
                    t.rmenu = new BootstrapMenu(t.renderto,{
                        fetchElementData: function(rowElem) {
                            return rowElem;
                        },
                        actions:t.actions,
                        actionsGroups:t.actionsGroups
                    });
                }catch(e){}
                t.trigger('inited',[t]);
            },
            showtext:function(b){//是否显示文字
                var t = this;
                if(b){
                    $(t.renderto).find('.btntext').txt('');
                }else{
                    t.buttons.forEach(function(btn){
                        btn.settext(btn.txt);
                    });
                }
            },
            insertbtn:function(index,btn){//插入按钮

            },
            appendbtn:function(){//新增按钮

            }
        });
        ex.RMenu = _RMenu;
    })(bsEx, window);

})(jQuery);

