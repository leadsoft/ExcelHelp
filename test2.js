$._DataTable = new DataTable();
//新增列
$._DataTable.column.add('第一列');
$._DataTable.column.add('第二列');
$._DataTable.column.add('第三列');
//数组可以新增
$._DataTable.column.add(["第四列","第五轮"]);
//new个新的行
var _newrow = $._DataTable.newRow();
//赋值
_newrow["第一列"] = "值1";
_newrow["第二列"] = "值2";
_newrow["第三列"] = "值3";
//新增行
$._DataTable.row.add(_newrow);
//查询
var row1 =  $._DataTable.row.select(m => m["第一列"]=='值1');
console.log($._DataTable.columns);
console.log(_newrow);
console.log($._DataTable.rows);
console.log(row1);
console.log($._DataTable.toJson());
