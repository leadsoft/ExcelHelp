if(!$._ExcelHelp){
  $._ExcelHelp = new ExcelHelp();
  //选择文件类型出错
  $._ExcelHelp.on('typeErr',function(){
    bsEx.Alert("提示","文件类型不正确。<br/>请重新选择文件",3);
  });
  //文件选择完成事件
  $._ExcelHelp.on('select',function(){
    bsEx.Alert("提示","文件打开了",3);
    //开始读取
    $._ExcelHelp.read(function(excelData,sheets){
      //第一页数据
      var sheet1 = excelData[0];
      //第二页数据
      var sheet2 = excelData[1];
      var newdata = [];
      //循环开始
      sheet2.forEach(function(item2){
          //查询name相等行 找一个是find 找数组是filter
         var rows = sheet1.find(function(item1){ return item2.name==item1.name });
         if(rows && rows.code){
              newdata.push(rows);
         }
      })
      console.log(newdata);
      //导出结果
      $._ExcelHelp.out("结果",newdata,[])
    })
  });
  //文件读取完成事件
  $._ExcelHelp.on('read',function(excelData,sheets){
    //第一页数据
    var sheet1 = excelData[0];
    console.log(sheets[0]+':'+sheet1);
    bsEx.Alert("提示","文件读取完了",3);
  });


}
//开始选择文件
$._ExcelHelp.select();