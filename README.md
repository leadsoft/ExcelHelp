# ExcelHelp

## 介绍
>* 基于JQ-Bootstrap和xlsx.core.min 实现前端读取导出Excel文件
>* 用于在项目现场处理Excel文件，解决部分实施工作量，测试浏览器为Firefox。
>* 开源地址 [https://gitee.com/shixixiyue/ExcelHelp](https://gitee.com/shixixiyue/ExcelHelp)
>* 主页 [https://shixixiyue.gitee.io/excelhelp/](https://shixixiyue.gitee.io/excelhelp/)
----
## 软件架构

>* 基于 [JQ-Bootstrap](https://shixixiyue.gitee.io/mybootstrapex) 和 [xlsx.core.min](http://www.jq22.com/jquery-info18298)

----
## 使用说明

>1. 引用excelHelp.js
>2. 创建对象var eh = new ExcelHelp();
>3. 注册两个关键事件 typeErr:文件选择错误事件；select:文件选择完成事件,见index.js
>4. 读取方法:eh.read；导出方法:eh.out，见index.html


> 属性名|类型|说明
> -|-|-
> fileType|数组|设置 文件类型 验证用 默认["xls","xlsx"]
> log|布尔|是否打开日志 默认true
> file|对象|当前选择的文件对象
> excelData|数组|当前读取的数据
> sheets|数组|当前读取的页签

> 方法名|参数|说明
> -|-|-
> select|无|执行选择文件方法，会弹出选择文件对护框，<br>选择前触发beforselect事件，<br>选择完成后触发select事件，<br>选择错误触发typeErr事件
> read|方法|文件读取方法，读取文件，参数为：(返回的数据,返回的页签名称)
> out|导出名称,数据(数组)|导出方法，把数据(json)，导出为Excel

> 事件|参数|说明
> -|-|-
> beforselect|无|文件选择前事件
> select|对象|文件选择完成事件，参数是选择的文件
> typeErr|无|文件选择出错事件
----
## 作者

>**北京@没想好**
----
